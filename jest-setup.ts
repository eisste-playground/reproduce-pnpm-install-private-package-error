import "@testing-library/jest-dom"
import fetch from "cross-fetch"
import { Logger, LogLevel } from "./src"

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
  clear: jest.fn(),
  key: jest.fn(),
  length: 5,
}

// noinspection JSConstantReassignment
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
global.localStorage = localStorageMock
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
global.fetch = fetch // required for jest node runtime - otherwise tested code using this will fail
Logger.level = LogLevel.silent
