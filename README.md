# Reproduce pnpm install private package error

When publishing a private package to GitLabs npm package registry I can not install it with pnpm, but I can do it with
npm and yarn.
Since pnpm is superior in terms of package resolution, installation speed and disk space usage I would love to be able
to use pnpm instead of yarn or npm.

## How to reproduce

1. Make a change to the code, so the package is built and automatically pushed to the registry.
2. Try to install the package with pnpm using a personal access token.
3. Getting the error described here: https://github.com/pnpm/pnpm/issues/3001
