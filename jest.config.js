/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  setupFilesAfterEnv: ["<rootDir>/jest-setup.ts"],
  testPathIgnorePatterns: [
    "<rootDir>/.next/",
    "<rootDir>/node_modules/",
    "<rootDir>/cypress/",
  ],
  moduleDirectories: ["<rootDir>", "node_modules"],
  moduleNameMapper: {
    "\\.(css|less|scss|sss|styl)$": "<rootDir>/node_modules/jest-css-modules",
    "\\.(gif|ttf|eot|svg)$": "<rootDir>/__mocks__/file-mock.js",
  },
  snapshotSerializers: ["@emotion/jest/serializer"],
  transformIgnorePatterns: ["/node_modules/(?!nanoid)"],
  testTimeout: 10000,
}
