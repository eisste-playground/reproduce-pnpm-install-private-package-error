import React from "react"
import theme from "../../utils/theme"

export interface ScreenDimensions {
  width: number
  height: number
}
export interface ScreenSize extends ScreenDimensions {
  isMobile: boolean
  isSmallMobile: boolean
  isBigMobile: boolean
  isTablet: boolean
  isSmallTablet: boolean
  isTabletOrMobileSize: boolean
  isTabletOrDesktop: boolean
  isDesktop: boolean
  isWidescreen: boolean
  isMobilePx360: boolean
  isMobilePx375: boolean
  isTinyTablet: boolean
}

export const buildScreenSizeObject = (
  screenDimensions: ScreenDimensions
): ScreenSize => {
  const width = screenDimensions.width

  const isMobile = width > 0 && width < theme.screenBreakpoint.sm
  const isSmallMobile =
    width > 0 && width < parseInt(theme.commonBreakpoints.px400, 10)
  const isMobilePx375 =
    width > 0 && width < parseInt(theme.commonBreakpoints.px375, 10)
  const isMobilePx360 =
    width > 0 && width < parseInt(theme.commonBreakpoints.px360, 10)
  const isBigMobile =
    width > 0 &&
    width >= parseInt(theme.commonBreakpoints.px360, 10) &&
    width < theme.screenBreakpoint.sm
  const isTablet =
    width > 0 &&
    width >= theme.screenBreakpoint.sm &&
    width < theme.screenBreakpoint.lg
  const isSmallTablet =
    width >= theme.screenBreakpoint.sm && width < theme.screenBreakpoint.md
  const isTinyTablet =
    width >= theme.screenBreakpoint.sm &&
    width < parseInt(theme.commonBreakpoints.px680, 10)
  const isTabletOrMobileSize = width > 0 && width < theme.screenBreakpoint.lg
  const isTabletOrDesktop = width >= theme.screenBreakpoint.sm
  const isDesktop =
    width === 0 /* Default for first render cycle */ ||
    width >= theme.screenBreakpoint.lg
  const isWidescreen = width > theme.screenBreakpoint.xl

  return {
    width: screenDimensions.width,
    height: screenDimensions.height,
    isSmallMobile,
    isBigMobile,
    isMobile,
    isMobilePx360,
    isMobilePx375,
    isTablet,
    isSmallTablet,
    isTinyTablet,
    isTabletOrMobileSize,
    isTabletOrDesktop,
    isDesktop,
    isWidescreen,
  }
}

export const ScreenSizeContext = React.createContext<ScreenSize>(
  buildScreenSizeObject({ width: 0, height: 0 })
)
