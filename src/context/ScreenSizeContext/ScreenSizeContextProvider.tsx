import React, { FC, useEffect, useState } from "react"
import {
  buildScreenSizeObject,
  ScreenDimensions,
  ScreenSizeContext,
} from "./ScreenSizeContext"

export const ScreeSizeContextProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [windowSize, setWindowSize] = useState<ScreenDimensions>({
    width: 0,
    height: 0,
  })

  useEffect(() => {
    const handleResize = () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      })
    }
    window.addEventListener("resize", handleResize)
    handleResize()
  }, [])

  return (
    <ScreenSizeContext.Provider value={buildScreenSizeObject(windowSize)}>
      {children}
    </ScreenSizeContext.Provider>
  )
}
