import React from "react"
import { TrackingTokenPayload } from "../utils"

export const TrackingTokenContext =
  React.createContext<TrackingTokenPayload | null>(null)
