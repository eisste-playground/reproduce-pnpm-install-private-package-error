export interface ClassName {
  /**
   * Support changes to component CSS from outside without the need of inline styles
   */
  className?: string
}

export interface AnchorLink {
  /**
   * Set true if your text should be a link
   */
  isAnchorLink?: boolean
  /**
   * Set url, where it should lead after text will be clicked
   */
  href?: string
}