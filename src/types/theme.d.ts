import {
  Colors,
  ScreenBreakpoint,
  SpaceAlias,
  Variants,
} from "../utils/theme.types"
import {
  CommonBreakpoints,
  FontWeight,
  FontSize,
  FontSizePrint,
  LineHeight,
  LineHeightPrint,
} from "../utils"

declare module "@mui/material/styles" {
  interface Theme {
    baseFontSize: string
    colorPalette: Colors
    spaceAlias: SpaceAlias
    spaceAliasPrint: SpaceAlias
    variants: Variants
    screenBreakpoint: ScreenBreakpoint
    commonBreakpoints: CommonBreakpoints
    fontWeight: FontWeight
    fontSize: FontSize
    fontSizePrint: FontSizePrint
    font: Array<string>
    lineHeight: LineHeight
    lineHeightPrint: LineHeightPrint
  }

  interface ThemeOptions {
    baseFontSize: string
    colorPalette: Colors
    spaceAlias: SpaceAlias
    spaceAliasPrint: SpaceAlias
    variants: Variants
    screenBreakpoint: ScreenBreakpoint
    commonBreakpoints: CommonBreakpoints
    fontWeight: FontWeight
    fontSize: FontSize
    fontSizePrint: FontSizePrint
    font: Array<string>
    lineHeight: LineHeight
    lineHeightPrint: LineHeightPrint
  }
}
