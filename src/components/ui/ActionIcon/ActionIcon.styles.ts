import styled from "@emotion/styled"
import { rgba } from "polished"
import theme from "../../../utils/theme"
import { IconProps } from "./ActionIcon.types"

export const StyledActionIcon = styled.button<IconProps>`
  display: inline-flex;
  height: 3em;
  width: 3em;
  font-size: 1em;
  border: 3px solid transparent;
  background-color: inherit;
  justify-content: center;
  align-items: center;
  & svg {
    font-size: 1.5em;
  }
  &:not(:disabled):not(.disabled) {
    cursor: pointer;
  }
  &:focus {
    outline: 0;
  }
  &:focus:not(:focus-visible) {
    outline: 0;
  }

  /* emphasis */
  ${({ emphasis, outline }) =>
    emphasis === "low" &&
    `
      color: ${
        outline
          ? theme.colorPalette.anthracite.default
          : theme.colorPalette.midGrey.default
      };
      border-color: ${
        outline ? theme.colorPalette.midGrey.default : "transparent"
      };
      &:hover {
        color: ${
          outline
            ? theme.colorPalette.white.hover
            : theme.colorPalette.anthracite.hover
        };
        background-color: ${
          outline ? theme.colorPalette.anthracite.hover : "transparent"
        };
        border-color: ${
          outline ? theme.colorPalette.anthracite.hover : "transparent"
        };
      }
      &:focus{
        outline: 0;
        box-shadow: 0 0 0 0.25rem ${rgba(
          theme.colorPalette.anthracite.hover,
          0.5
        )};
      }
    `}

  ${({ emphasis, outline }) =>
    emphasis === "medium" &&
    `
      color: ${
        outline
          ? theme.colorPalette.anthracite.default
          : theme.colorPalette.midGrey.default
      };
      border-color: ${
        outline ? theme.colorPalette.midGrey.default : "transparent"
      };
      &:hover {
        color: ${
          outline
            ? theme.colorPalette.white.hover
            : theme.variants.primary.hover
        };
        background-color: ${
          outline ? theme.colorPalette.anthracite.hover : "transparent"
        };
        border-color: ${
          outline ? theme.colorPalette.anthracite.hover : "transparent"
        };
      }
      &:focus{
        outline: 0;
        &:focus{
        outline: 0;
        box-shadow: 0 0 0 0.25rem ${
          outline
            ? rgba(theme.colorPalette.anthracite.hover, 0.5)
            : rgba(theme.variants.primary.hover, 0.5)
        };
      }
      }
    `}

    ${({ emphasis, outline }) =>
    emphasis === "high" &&
    `
      color: ${theme.colorPalette.anthracite.default};
      border-color: ${
        outline ? theme.colorPalette.midGrey.default : "transparent"
      };
      &:hover {
        color: ${
          outline
            ? theme.colorPalette.anthracite.hover
            : theme.variants.primary.hover
        };
        background-color: "transparent";
        border-color: ${outline ? theme.variants.primary.hover : "transparent"};
      }
      &:focus{
        outline: 0;
        box-shadow: 0 0 0 0.25rem ${rgba(theme.variants.primary.hover, 0.5)};
      }
    `}

    /* Sizes */
       ${({ size }) =>
    size === "extra-small" &&
    `
      padding:0;
      height: 1.8em;
      width: 1.8em;
      & svg{
        font-size: 0.8em;
      }
    `}
    ${({ size }) =>
    size === "small" &&
    `
      padding:0;
      height: 2em;
      width: 2em;
      & svg{
        font-size: 1em;
      }
    `}
    ${({ size }) =>
    size === "large" &&
    `
      height: 4em;
      width: 4em;
      & svg{
        font-size: 2em;
      }
    `}

    /* Round */
    ${({ round }) =>
    round &&
    `
      border-radius: 50em;
    `}

    /* Disabled */
    ${({ disabled }) =>
    disabled &&
    `
      opacity: .35;
      cursor: not-allowed;
      pointer-events: none;
    `}
`
