import React from "react"
import { Story, Meta } from "@storybook/react"
import {
  faAtlas,
  faBasketballBall,
  faShoppingBag,
  faShoppingCart,
} from "@fortawesome/pro-light-svg-icons"
import { ActionIcon } from "./ActionIcon"
import { ActionIconProps } from "./ActionIcon.types"

export default {
  title: "Components/ActionIcon",
  component: ActionIcon,
} as Meta

const Template: Story<ActionIconProps> = (args) => <ActionIcon {...args} />

export const RoundAndOutline = Template.bind({})
RoundAndOutline.args = {
  ariaLabel: "Expected label",
  icon: faAtlas,
  emphasis: "low",
  round: true,
  outline: true,
}

export const LowEmphasis = Template.bind({})
LowEmphasis.args = {
  ariaLabel: "Expected label",
  icon: faShoppingCart,
  emphasis: "low",
}

export const MediumEmphasis = Template.bind({})
MediumEmphasis.args = {
  ariaLabel: "Expected label",
  icon: faShoppingBag,
  emphasis: "medium",
}

export const HighEmphasis = Template.bind({})
HighEmphasis.args = {
  ariaLabel: "Expected label",
  icon: faBasketballBall,
  emphasis: "high",
}

export const CustomIcon = Template.bind({})
CustomIcon.args = {
  ariaLabel: "Expected label",
  customIcon: "user",
  emphasis: "low",
  round: true,
  outline: true,
}
