import React from "react"
import { Story, Meta } from "@storybook/react"
import { faMoonStars } from "@fortawesome/pro-light-svg-icons"
import { ActionIconBase } from "./ActionIcon"
import { ActionIconBaseProps } from "./ActionIcon.types"

export default {
  title: "Components/ActionIcon/PrefabBase",
  component: ActionIconBase,
} as Meta

const TemplatePrefabBase: Story<ActionIconBaseProps> = (args) => (
  <ActionIconBase {...args} />
)

export const PrefabBase = TemplatePrefabBase.bind({})
PrefabBase.args = {
  ariaLabel: "Mond und Sterne",
  icon: faMoonStars,
}
