import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Icon from "./Icon/Icon"
import {
  ActionIconBaseProps,
  ActionIconOutlineProps,
  ActionIconProps,
} from "./ActionIcon.types"
import { StyledActionIcon } from "./ActionIcon.styles"

export const ActionIcon = ({
  icon,
  customIcon,
  ariaLabel,
  emphasis,
  round,
  outline,
  disabled,
  size,
  onClick,
  className,
}: ActionIconProps) => (
  <StyledActionIcon
    aria-label={ariaLabel}
    onClick={onClick}
    disabled={disabled}
    emphasis={emphasis}
    outline={outline}
    round={round}
    size={size}
    className={className}
  >
    {!customIcon && icon && <FontAwesomeIcon icon={icon} />}
    {customIcon && <Icon icon={customIcon} />}
  </StyledActionIcon>
)
ActionIcon.defaultProps = {
  size: "medium",
}

// Prefabs
export const ActionIconBase = (props: ActionIconBaseProps) => (
  <ActionIcon emphasis="medium" round {...props} />
)

export const ActionIconOutline = ({
  highlight = "low",
  ...rest
}: ActionIconOutlineProps) => (
  <ActionIcon emphasis={highlight} outline round {...rest} />
)
