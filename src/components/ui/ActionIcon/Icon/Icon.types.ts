export type IconName = "user"

export interface IconProp {
  icon: IconName
}