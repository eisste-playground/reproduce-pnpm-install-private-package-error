import styled from "@emotion/styled"

export const StyledSVG = styled.svg`
  display: inline-block;
  font-size: inherit;
  height: 1em;
  overflow: visible;
`
