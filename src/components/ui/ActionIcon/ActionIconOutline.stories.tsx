import React from "react"
import { Story, Meta } from "@storybook/react"
import { faRocketLaunch } from "@fortawesome/pro-light-svg-icons"
import { ActionIconOutline } from "./ActionIcon"
import { ActionIconOutlineProps } from "./ActionIcon.types"

export default {
  title: "Components/ActionIcon/PrefabOutline",
  component: ActionIconOutline,
} as Meta

const TemplatePrefabOutline: Story<ActionIconOutlineProps> = (args) => (
  <ActionIconOutline {...args} />
)

export const PrefabOutlineLow = TemplatePrefabOutline.bind({})
PrefabOutlineLow.args = {
  ariaLabel: "Auf gehts!",
  icon: faRocketLaunch,
  highlight: "low",
}

export const PrefabOutlineHigh = TemplatePrefabOutline.bind({})
PrefabOutlineHigh.args = {
  ariaLabel: "Accounteinstellungen",
  customIcon: "user",
  highlight: "high",
}
