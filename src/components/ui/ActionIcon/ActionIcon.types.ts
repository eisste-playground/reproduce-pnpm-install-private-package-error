import React from "react"
import { IconDefinition } from "@fortawesome/fontawesome-common-types"
import { IconName } from "./Icon"

interface AriaProps {
  /**
   * Aria-Label
   */
  ariaLabel: string
}
interface BaseProps {
  /**
   * Icon from fontawesome
   */
  icon?: IconDefinition
  /**
   * Icon from own
   */
  customIcon?: IconName

  /**
   * Set the size of
   */
  size?: "extra-small" | "small" | "medium" | "large"
  /**
   * Checks – if the button should get disabled
   */
  disabled?: boolean
  /**
   * Event – on click
   */
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void
}

// IconProps without aria props so we can use original aria-attributes on the actual <button> element
export interface IconProps extends BaseProps {
  /**
   * Set the colorway of component
   */
  emphasis: "low" | "medium" | "high"
  /**
   * Checks – if button should get rounded
   */
  round?: boolean
  /**
   * Checks – if button should get the outline version
   */
  outline?: boolean
}
export interface ActionIconProps extends AriaProps, IconProps {
  className?: string
}

export interface ActionIconBaseProps extends AriaProps, BaseProps {}
export interface ActionIconOutlineProps extends AriaProps, BaseProps {
  /**
   * Set the highlight of the component
   * Defaults to "low".
   */
  highlight?: "low" | "high"
}
