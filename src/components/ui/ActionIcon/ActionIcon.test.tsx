/**
 * @jest-environment jsdom
 */
import React from "react"
import { matchers } from "@emotion/jest"
import {
  faChevronDoubleRight,
  faMoonStars,
  faRocketLaunch,
  faShoppingCart,
} from "@fortawesome/pro-light-svg-icons"
import { render, fireEvent, screen } from "../../../utils/test-utils"
import theme from "../../../utils/theme"
import { ActionIcon, ActionIconBase, ActionIconOutline } from "./ActionIcon"

// Add the custom matchers provided by "@emotion/jest"
expect.extend(matchers)

describe("<ActionIcon> component", () => {
  it("rendered with aria-label correctly", () => {
    const ariaLabel = "Add to cart"
    const { container } = render(
      <ActionIcon ariaLabel={ariaLabel} emphasis="low" icon={faShoppingCart} />
    )
    const btn = container.querySelector("button")
    expect(btn).toBeTruthy()
    expect(btn).toHaveAttribute("aria-label", ariaLabel)
  })

  it("rendered fontawesome-svg correctly", () => {
    const iconNameAsString = "fa-cart-shopping"
    const { container } = render(
      <ActionIcon
        ariaLabel="Add to cart"
        emphasis="low"
        icon={faShoppingCart}
      />
    )
    const svg = container.querySelector('[role="img"]')
    expect(svg).toBeTruthy()
    expect(svg?.getAttribute("class")).toMatch(iconNameAsString)

    const svgPath = container.querySelector('[role="img"] path')
    expect(svgPath).toBeTruthy()
  })

  it("click event successfully triggered", () => {
    let clickValue = 0
    render(
      <ActionIcon
        ariaLabel="Continue"
        emphasis="low"
        icon={faChevronDoubleRight}
        onClick={() => clickValue++}
      />
    )
    fireEvent.click(screen.getByRole("button"))
    fireEvent.click(screen.getByRole("button"))
    expect(clickValue).toBe(2)
  })

  it("has functional disabled attribute", () => {
    let clickValue = 0
    render(
      <ActionIcon
        ariaLabel="Continue"
        emphasis="low"
        icon={faChevronDoubleRight}
        onClick={() => clickValue++}
        disabled
      />
    )
    expect(screen.getByRole("button")).toHaveAttribute("disabled")
    fireEvent.click(screen.getByRole("button"))
    expect(clickValue).toBe(0)
  })

  describe("<ActionIcon> Prefabs", () => {
    it("<ActionIconBase> renders correctly", () => {
      const label = "Mond und Sterne"
      const { container } = render(
        <ActionIconBase ariaLabel={label} icon={faMoonStars} />
      )
      const svg = container.querySelector('[role="img"]')
      expect(svg).toHaveClass("fa-moon-stars")
      const svgPath = container.querySelector('[role="img"] path')
      expect(svgPath).toBeTruthy()
      const btn = container.querySelector("button")
      expect(btn).toHaveAttribute("aria-label", expect.stringContaining(label))
    })
    it("<ActionIconOutline> with low highlighting renders correctly", () => {
      const label = "Auf gehts!"
      const { container } = render(
        <ActionIconOutline
          highlight="low"
          ariaLabel={label}
          icon={faRocketLaunch}
        />
      )
      const svg = container.querySelector('[role="img"]')
      expect(svg).toHaveClass("fa-rocket-launch")
      const svgPath = container.querySelector('[role="img"] path')
      expect(svgPath).toBeTruthy()
      const btn = container.querySelector("button")
      expect(btn).toHaveAttribute("aria-label", expect.stringContaining(label))
      expect(container.firstChild).toHaveStyleRule(
        "border-color",
        theme.colorPalette.midGrey.default
      )
      expect(container.firstChild).toHaveStyleRule(
        "border-color",
        theme.colorPalette.anthracite.hover,
        {
          target: ":hover",
        }
      )
      expect(container.firstChild).toHaveStyleRule(
        "background-color",
        theme.colorPalette.anthracite.hover,
        {
          target: ":hover",
        }
      )
    })
    it("<ActionIconOutline> with high highlighting renders correctly", () => {
      const label = "Accounteinstellungen"
      const { container } = render(
        <ActionIconOutline
          highlight="high"
          ariaLabel={label}
          customIcon="user"
        />
      )
      const svg = container.querySelector('[role="img"]')
      expect(svg).toHaveAttribute("data-icon", expect.stringContaining("user"))
      const svgPath = container.querySelector('[role="img"] path')
      expect(svgPath).toBeTruthy()
      const btn = container.querySelector("button")
      expect(btn).toHaveAttribute("aria-label", expect.stringContaining(label))
      expect(container.firstChild).toHaveStyleRule(
        "border-color",
        theme.colorPalette.midGrey.default
      )
      expect(container.firstChild).toHaveStyleRule(
        "border-color",
        theme.variants.primary.hover,
        {
          target: ":hover",
        }
      )
    })
  })
})
