import { useState, useEffect, useRef, MutableRefObject } from "react"

export const useComponentVisible = (
  initialIsVisible: boolean,
  ignoredElementCssSelector?: string
) => {
  const [isComponentVisible, setIsComponentVisible] = useState(initialIsVisible)
  const ref = useRef() as MutableRefObject<HTMLDivElement>

  const handleClickOutside = (event: Event) => {
    const notIgnoredElement =
      ignoredElementCssSelector &&
      event.target instanceof Element &&
      !event.target.closest(ignoredElementCssSelector)

    if (
      ref.current &&
      !ref.current.contains(event.target as Node) &&
      notIgnoredElement
    ) {
      setIsComponentVisible(false)
    }
  }

  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true)
    return () => {
      document.removeEventListener("click", handleClickOutside, true)
    }
  })

  useEffect(() => {
    document.addEventListener("keydown", (e) => {
      // eslint-disable-next-line no-unused-expressions
      e.key === "Escape" && setIsComponentVisible(false)
    })

    return () => {
      document.removeEventListener("keydown", (e) => e)
    }
  }, [])

  return { ref, isComponentVisible, setIsComponentVisible }
}
