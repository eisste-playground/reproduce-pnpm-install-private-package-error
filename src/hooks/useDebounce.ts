import { useState, useEffect } from "react"

// "unknown" can't do the same for us as any, "never" is no option here either. At this moment we think we need any here.
export const useDebounce = (value: any, delay: number) => {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)
    return () => {
      clearTimeout(handler)
    }
  }, [delay, value])

  return debouncedValue
}
