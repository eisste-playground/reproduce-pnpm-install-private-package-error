/* eslint-disable @typescript-eslint/no-unused-vars */
// eslint-disable-next-line max-classes-per-file
import pino from "pino"

import {
  isRuntimeEnvironmentClient,
  isRuntimeEnvironmentServer,
} from "../runtime-environment-checks"
import {
  LogEnvironment,
  LoggerOptions,
  LogLevel,
  LogLevelString,
} from "./Logger.types"

class SentryLogger {
  private readonly level: LogLevel

  constructor(level: LogLevel) {
    this.level = level
  }

  captureMessageByLevel(_msg: string): void {
    const severity = SentryLogger.mapLogLevelToSentrySeverity(this.level)
    if (severity === null) {
      // eslint-disable-next-line no-useless-return
      return
    }
  }

  captureExceptionByLevel(_exception: Error): void {
    const severity = SentryLogger.mapLogLevelToSentrySeverity(this.level)
    // severity is null when level is "silent"
    if (severity === null) {
      // eslint-disable-next-line no-useless-return
      return
    }
  }

  private static mapLogLevelToSentrySeverity(level: LogLevel): null {
    switch (level) {
      case LogLevel.silent:
        return null
      case LogLevel.trace:
        return null
      case LogLevel.debug:
        return null
      case LogLevel.info:
        return null
      case LogLevel.warn:
        return null
      case LogLevel.error:
        return null
      case LogLevel.fatal:
        return null
      default:
        return null
    }
  }
}

/**
 * level omits log messages aiming at log levels below the configured level.
 * pinoLogEnvironment defines where to use pino logging: clientside, node (server side), or both.
 */
export class Logger {
  public static level: LogLevel = LogLevel.error
  public static pinoLogEnvironment: LogEnvironment = "node"
  private readonly pinoLogger: pino.Logger = pino({
    level: Logger.mapLogLevelToPinoLogLevel(Logger.level),
  })
  private readonly sentryLogger: SentryLogger = new SentryLogger(Logger.level)

  trace(msg: string, err?: Error) {
    if (Logger.level !== LogLevel.silent && Logger.level === LogLevel.trace) {
      this.log(LogLevel.trace, msg, err)
    }
  }

  debug(msg: string, err?: Error) {
    // level trace, debug
    if (Logger.level !== LogLevel.silent && Logger.level <= LogLevel.debug) {
      this.log(LogLevel.debug, msg, err)
    }
  }

  info(msg: string, err?: Error) {
    // log level trace, debug, info
    if (Logger.level !== LogLevel.silent && Logger.level <= LogLevel.info) {
      this.log(LogLevel.info, msg, err)
    }
  }

  warn(msg: string, err?: Error) {
    // log level trace, debug, info, warn
    if (Logger.level !== LogLevel.silent && Logger.level <= LogLevel.warn) {
      this.log(LogLevel.warn, msg, err)
    }
  }

  error(msg: string, err?: Error) {
    // log level trace, debug, info, warn, error
    if (Logger.level !== LogLevel.silent && Logger.level <= LogLevel.error) {
      this.log(LogLevel.error, msg, err)
    }
  }

  fatal(msg: string, err?: Error) {
    // log level trace, debug, info, warn, error
    if (Logger.level !== LogLevel.silent && Logger.level <= LogLevel.fatal) {
      this.log(LogLevel.fatal, msg, err)
    }
  }

  public static getLogLevelFromString(levelString: LogLevelString): LogLevel {
    switch (levelString) {
      case "silent":
        return LogLevel.silent
      case "trace":
        return LogLevel.trace
      case "debug":
        return LogLevel.debug
      case "info":
        return LogLevel.info
      case "warn":
        return LogLevel.warn
      case "error":
        return LogLevel.error
      case "fatal":
        return LogLevel.fatal
      default:
        return LogLevel.warn
    }
  }

  public static getPinoEnvironmentFromString(
    environment: string
  ): LogEnvironment {
    const possibleEnvironments: string[] = ["node", "client", "both"]
    if (possibleEnvironments.includes(environment)) {
      return environment as LogEnvironment
    }
    return "node"
  }

  private log(level: LogLevel, msg: string, err?: Error) {
    switch (Logger.pinoLogEnvironment) {
      case "client": {
        if (isRuntimeEnvironmentClient()) {
          this.executeMatchingPinoLogMethodByLevel(level, msg)
        }
        break
      }
      case "node": {
        if (isRuntimeEnvironmentServer()) {
          this.executeMatchingPinoLogMethodByLevel(level, msg)
        }
        break
      }
      case "both":
      default:
        this.executeMatchingPinoLogMethodByLevel(level, msg)
        break
    }
    this.sentryLogger.captureMessageByLevel(msg)
    if (err) {
      this.sentryLogger.captureExceptionByLevel(err)
    }
  }

  private executeMatchingPinoLogMethodByLevel(
    level: LogLevel,
    msg: string
  ): void {
    switch (level) {
      case LogLevel.silent:
        return this.pinoLogger.silent(msg)
      case LogLevel.trace:
        return this.pinoLogger.trace(msg)
      case LogLevel.debug:
        return this.pinoLogger.debug(msg)
      case LogLevel.info:
        return this.pinoLogger.info(msg)
      case LogLevel.warn:
        return this.pinoLogger.warn(msg)
      case LogLevel.error:
        return this.pinoLogger.error(msg)
      case LogLevel.fatal:
        return this.pinoLogger.fatal(msg)
      default:
        return this.pinoLogger.warn(msg)
    }
  }

  private static mapLogLevelToPinoLogLevel(
    level: LogLevel
  ): pino.Level | "silent" {
    switch (level) {
      case LogLevel.silent:
        return "silent"
      case LogLevel.trace:
        return "trace"
      case LogLevel.debug:
        return "debug"
      case LogLevel.info:
        return "info"
      case LogLevel.warn:
        return "warn"
      case LogLevel.error:
        return "error"
      case LogLevel.fatal:
        return "fatal"
      default:
        return "warn"
    }
  }
}

/**
 * Creates an instance of Logger.
 *
 * Defaults to environment variable values.
 * If these aren't set level defaults to "warn" and pino runtime environment to "node".
 *
 * @param options?: LoggerOptions
 */
const createLogger = (options?: LoggerOptions): Logger => {
  Logger.level =
    options?.level ||
    Logger.getLogLevelFromString(
      (process.env.NEXT_PUBLIC_LOGLEVEL as LogLevelString) || "warn"
    )
  Logger.pinoLogEnvironment =
    options?.pinoRuntimeEnvironment ||
    Logger.getPinoEnvironmentFromString(
      process.env.NEXT_PUBLIC_PINO_RUNTIME_ENVIRONMENT || "node"
    )
  return new Logger()
}

// eslint-disable-next-line no-restricted-exports
export { createLogger as default }
