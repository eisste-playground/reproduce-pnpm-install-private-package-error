// number values taken from pino level docs to roughly match what they do
export enum LogLevel {
  trace = 10,
  debug = 20,
  info = 30,
  warn = 40,
  error = 50,
  fatal = 60,
  silent = Infinity,
}

export type LogLevelString =
  | "trace"
  | "debug"
  | "info"
  | "warn"
  | "error"
  | "fatal"
  | "silent"

export type LogEnvironment = "node" | "client" | "both"

export interface LoggerOptions {
  level?: LogLevel
  pinoRuntimeEnvironment?: LogEnvironment
}
