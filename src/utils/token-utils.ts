// Docs to JWT registered claims https://datatracker.ietf.org/doc/html/rfc7519#section-4.1
import { isRuntimeEnvironmentClient } from "./runtime-environment-checks"

interface JWTRegisteredClaims {
  iss?: string
  sub?: string
  aud?: string
  exp?: number // NumericDate -> number of seconds from 1970-01-01T00:00:00Z
  nbf?: number // NumericDate -> number of seconds from 1970-01-01T00:00:00Z
  iat?: number // NumericDate -> number of seconds from 1970-01-01T00:00:00Z
  jti?: string // case sensitive
}

interface TrackingTokenClaims extends JWTRegisteredClaims {
  iss: string // host
  iat: number // issued at NumericDate -> number of seconds from 1970-01-01T00:00:00Z
  sub: string // session id
}

export interface TrackingTokenPayload {
  sessionID: string // uuid
}

export const getTrackingTokenPayloadFromLocalstorage =
  (): TrackingTokenPayload | null => {
    if (!isRuntimeEnvironmentClient()) {
      throw new Error(
        "TrackingToken can only be read from localstorage on client side."
      )
    }

    // Token not set, empty or invalid? return null
    const rawTokenPossiblyNull = localStorage.getItem("token")
    if (!isValidTrackingToken(rawTokenPossiblyNull)) {
      return null
    }

    // We already know this is not null, because it's checked above.
    const rawToken: string = rawTokenPossiblyNull as string

    // token set? build token object and return it
    const jwtTracking: TrackingTokenClaims = JSON.parse(
      atob(rawToken.split(".")[1])
    )
    return {
      sessionID: jwtTracking.sub,
    }
  }

export const isJsonStringValid = (str: string): boolean => {
  try {
    JSON.parse(str)
  } catch (e) {
    return false
  }
  return true
}

export const isValidTrackingToken = (jwtString: string | null) => {
  if (jwtString === null || jwtString.length === 0) {
    return false
  }
  const splitString = jwtString.split(".")
  if (splitString.length !== 3) {
    return false
  }
  const rawJwtPayload = atob(splitString[1])
  if (isJsonStringValid(rawJwtPayload)) {
    const possiblyValidTrackingToken = JSON.parse(rawJwtPayload)
    return (
      Object.prototype.hasOwnProperty.call(possiblyValidTrackingToken, "sub") && // session id
      Object.prototype.hasOwnProperty.call(possiblyValidTrackingToken, "iat") && // issued at NumericDate -> number of seconds from 1970-01-01T00:00:00Z
      Object.prototype.hasOwnProperty.call(possiblyValidTrackingToken, "iss") // host
    )
  }
  return false
}
