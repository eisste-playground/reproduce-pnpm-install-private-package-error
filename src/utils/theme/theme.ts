import { createTheme } from "@mui/material/styles"
import { deDE } from "@mui/material/locale"
import { Colors, ScreenBreakpoint, SpaceAlias, Variants } from "./theme.types"

const space: Array<number | string> = [
  0,
  "0.25em",
  "0.5em",
  "1em",
  "1.5em",
  "2em",
  "2.5em",
  "3em",
  "3.5em",
  "4em",
  "4.5em",
]

// All sizes in px refer to a font-size of 16px. Will differ for other font-sizes.
const spaceAlias: SpaceAlias = {
  none: space[0], // 0px
  xxs: space[1], // 4px
  xs: space[2], // 8px
  sm: space[3], // 16px
  md: space[4], // 24px
  lg: space[5], // 32px
  xl: space[6], // 40px
  xxl: space[7], // 48px
  xxxl: space[8], // 56px
  xxxxl: space[9], // 64px
  xxxxxl: space[10], // 72px
}

const spacePrint: Array<number | string> = [
  0,
  "2pt",
  "3pt",
  "5pt",
  "6pt",
  "7pt",
  "8pt",
  "10pt",
  "12pt",
  "13pt",
  "15pt",
]

const spaceAliasPrint: SpaceAlias = {
  none: spacePrint[0],
  xxs: spacePrint[1], // 2pt
  xs: spacePrint[2], // 3pt
  sm: spacePrint[3], // 5pt
  md: spacePrint[4], // 6pt
  lg: spacePrint[5], // 7pt
  xl: spacePrint[6], // 8pt
  xxl: spacePrint[7], // 10pt
  xxxl: spacePrint[8], // 12pt
  xxxxl: spacePrint[9], // 13pt
  xxxxxl: spacePrint[10], // 15pt
}

/*
  Styled system's first breakpoint is implicit 0.

  Also they require every breakpoint to be a css string with size unit (px, em, ...).
  Unfortunately this is not documented in the official docs - but now we documented it here!
 */
const breakpoints = ["600px", "960px", "1280px", "1920px"]

export interface CommonBreakpoints {
  px360: string
  px375: string
  px400: string
  px500: string
  px650: string
  px680: string
  px820: string
  px1440: string
}

const commonBreakpoints: CommonBreakpoints = {
  px360: "360px",
  px375: "375px",
  px400: "400px",
  px500: "500px",
  px680: "680px",
  px650: "650px",
  px820: "820px",
  px1440: "1440px",
}

// Read docblock above const breakpoints's declaration to see why we need to use parseInt and set 0 literally
const screenBreakpoint: ScreenBreakpoint = {
  xs: 0,
  sm: parseInt(breakpoints[0], 10),
  md: parseInt(breakpoints[1], 10),
  lg: parseInt(breakpoints[2], 10),
  xl: parseInt(breakpoints[3], 10),
}

const colorPalette: Colors = {
  red: {
    default: "#e2001a",
    hover: "#ff001d",
  },
  grey: {
    default: "#f0f0f0",
    hover: "#f6f6f6",
  },
  midGrey: {
    default: "#dedede",
    hover: "#efefef",
  },
  lightGrey: {
    default: "#8e8e8e",
    hover: "#9f9f9f",
  },
  darkGrey: {
    default: "#282828",
    hover: "#393939",
  },
  green: {
    default: "#28A858",
    hover: "#3ebe6e",
  },
  blue: {
    default: "#4a90e2",
    hover: "#5ba1f3",
  },
  seagreen: {
    default: "#1f9e93",
    hover: "#2fafa4",
  },
  yellow: {
    default: "#f9d026",
    hover: "#faf147",
  },
  orange: {
    default: "#f5a623",
    hover: "#f6b734",
  },
  white: {
    default: "#ffffff",
    hover: "#efefef",
  },
  anthracite: {
    default: "#282828",
    hover: "#1d1d1d",
  },
  black: {
    default: "#000000",
    hover: "#1d1d1d",
  },
  purple: {
    default: "#753CA7",
    hover: "#61328a",
  },
}

const variants: Variants = {
  primary: {
    default: colorPalette.red.default,
    hover: colorPalette.red.hover,
    text: colorPalette.white.default,
    textHover: colorPalette.white.default,
    outlineText: colorPalette.red.default,
    outlineTextHover: colorPalette.red.hover,
  },
  secondary: {
    default: colorPalette.midGrey.default,
    hover: colorPalette.midGrey.hover,
    text: colorPalette.darkGrey.default,
    textHover: colorPalette.darkGrey.hover,
    outlineText: colorPalette.darkGrey.default,
    outlineTextHover: colorPalette.darkGrey.hover,
  },
  success: {
    default: colorPalette.green.default,
    hover: colorPalette.green.hover,
    text: colorPalette.white.default,
    textHover: colorPalette.white.default,
    outlineText: colorPalette.green.default,
    outlineTextHover: colorPalette.green.hover,
  },
  danger: {
    default: colorPalette.red.default,
    hover: colorPalette.red.hover,
    text: colorPalette.white.default,
    textHover: colorPalette.white.default,
    outlineText: colorPalette.red.default,
    outlineTextHover: colorPalette.red.hover,
  },
  warning: {
    default: colorPalette.orange.default,
    hover: colorPalette.orange.hover,
    text: colorPalette.white.default,
    textHover: colorPalette.white.default,
    outlineText: colorPalette.orange.default,
    outlineTextHover: colorPalette.orange.hover,
  },
  info: {
    default: colorPalette.blue.default,
    hover: colorPalette.blue.hover,
    text: colorPalette.white.default,
    textHover: colorPalette.white.default,
    outlineText: colorPalette.blue.default,
    outlineTextHover: colorPalette.blue.hover,
  },
  anthracite: {
    default: colorPalette.anthracite.default,
    hover: colorPalette.anthracite.hover,
    text: colorPalette.white.default,
    textHover: colorPalette.white.default,
    outlineText: colorPalette.anthracite.default,
    outlineTextHover: colorPalette.anthracite.hover,
  },
}

export interface FontSize {
  px14: string
  px16: string
  px18: string
  px20: string
  px24: string
  px26: string
  px38: string
}

const fontSize: FontSize = {
  px14: "14px",
  px16: "16px",
  px18: "18px",
  px20: "20px",
  px24: "24px",
  px26: "26px",
  px38: "38px",
}

export interface FontSizePrint {
  pt7: string
  pt8: string
  pt10: string
  pt11: string
  pt12: string
  pt13: string
  pt17: string
}

const fontSizePrint: FontSizePrint = {
  pt7: "7pt",
  pt8: "8pt",
  pt10: "10pt",
  pt11: "11pt",
  pt12: "12pt",
  pt13: "13pt",
  pt17: "17pt",
}

const font: Array<string> = ["Roboto"]

export interface FontWeight {
  light: number
  regular: number
  bold: string
}

const fontWeight: FontWeight = {
  light: 300,
  regular: 400,
  bold: "bold",
}

export interface LineHeight {
  px23: string
  px25: string
  px26: string
  px29: string
  px33: string
  px36: string
  px50: string
}

const lineHeight: LineHeight = {
  px23: "23px",
  px25: "25px",
  px26: "26px",
  px29: "29px",
  px33: "33px",
  px36: "36px",
  px50: "50px",
}

export interface LineHeightPrint {
  pt13: string
  pt18: string
  pt20: string
}

const lineHeightPrint: LineHeightPrint = {
  pt13: "13pt",
  pt18: "18pt",
  pt20: "20pt",
}

const theme = createTheme(
  {
    baseFontSize: fontSize.px16, // 16px
    colorPalette,
    font,
    fontSize,
    fontSizePrint,
    fontWeight,
    lineHeight,
    lineHeightPrint,
    spaceAlias,
    spaceAliasPrint,
    variants,
    screenBreakpoint,
    commonBreakpoints,
    palette: {
      primary: {
        main: colorPalette.red.default,
        light: colorPalette.red.hover,
        dark: colorPalette.red.default,
      },
      secondary: {
        main: colorPalette.darkGrey.default,
        light: colorPalette.darkGrey.hover,
      },
      success: {
        main: colorPalette.green.default,
        light: colorPalette.green.hover,
      },
      warning: {
        main: colorPalette.orange.default,
        light: colorPalette.orange.hover,
      },
      info: {
        main: colorPalette.blue.default,
        light: colorPalette.blue.hover,
      },
    },
    typography: {
      fontFamily: font[0] + ",Open Sans,Droid Sans,sans-serif",
      fontSize: parseInt(fontSize.px16, 10),
    },
  },
  deDE
)

export default theme
