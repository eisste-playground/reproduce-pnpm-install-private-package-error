import {
  isRuntimeEnvironmentClient,
  isRuntimeEnvironmentServer,
} from "./runtime-environment-checks"

const localDevReferer = "www.waf-seminar.test"

/**
 * Builds the referer value we send in the "X-Referer" header from window.location for client side
 * Throws an error when js runtime environment is not client.
 *
 * Schema: ${protocolIncludingColon}//www.${hostname}:?${port}.
 * E.g. http://www.waf-seminar.test:3000, or https://www.waf-seminar.de
 *
 * @throws Error
 * @returns string
 */
export const buildRefererForClient = (): string => {
  if (!isRuntimeEnvironmentClient()) {
    throw new Error(
      "Can't build referer from window.location when the runtime environment is anything other than a browser client!"
    )
  }

  const protocol = window.location.protocol
  const hostname =
    window.location.hostname === "localhost"
      ? localDevReferer
      : window.location.hostname
  const port = window.location.port

  return `${protocol}//${
    hostname.startsWith("www.") ? hostname : "www." + hostname
  }${port !== "80" && port !== "" ? ":" + port : ""}`
}

/**
 * Build the referer value we send in the "X-Referer" header from the requests host header.
 *
 * The header is filled automatically by Next.js and includes the port, but excludes the used protocol.
 * We use http: as protocol for hostname www.waf-seminar.test.
 * All other hosts get https: as protocol.
 *
 *
 * @param host
 * @throws Error
 * @returns string
 */
export const buildRefererForNextjsServerEnvironment = (
  host: string
): string => {
  if (!isRuntimeEnvironmentServer()) {
    throw new Error(
      "Can't build referer from window.location when the runtime environment is anything other than a browser client!"
    )
  }
  const hostWithPort = host.startsWith("localhost")
    ? `${localDevReferer}:3000`
    : host
  const protocol = hostWithPort.includes(localDevReferer) ? "http" : "https"
  return `${protocol}://${hostWithPort}`
}
