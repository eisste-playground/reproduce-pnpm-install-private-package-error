// React testing library setup.

// Here we can add all things needed to be wrapped around components that we need to test.
// If e.g. a ThemeProvider holding a theme is used for the actual app it is a good idea to wrap it around the tested components on render, too.
// This setup re-exports everything in @testing-library/react and should be used as drop-in replacement.

// So use this:
// `import { render, fireEvent } from "utils/test-utils"`
// Instead of this:
// `import { render, fireEvent } from "@testing-library/react"`

import React, { FC, ReactElement } from "react"
import {
  queries,
  Queries,
  render,
  RenderOptions,
  RenderResult,
} from "@testing-library/react"
import { ThemeProvider } from "@mui/material/styles"
import {
  buildScreenSizeObject,
  ScreenDimensions,
  ScreenSizeContext,
} from "../context/ScreenSizeContext/ScreenSizeContext"
import theme from "./theme"

const AllTheProviders: FC<{ children: React.ReactNode }> = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
)

function customRender<
  Q extends Queries = typeof queries,
  Container extends Element | DocumentFragment = HTMLElement,
  BaseElement extends Element | DocumentFragment = Container
>(
  ui: ReactElement,
  options?: Omit<RenderOptions<Q, Container, BaseElement>, "queries">
): RenderResult<Q, Container, BaseElement> {
  return render(ui, { wrapper: AllTheProviders, ...options })
}
export * from "@testing-library/react"
export { customRender as render }

export const renderScreenSizeContextConsumer = (
  ui: React.ReactElement,
  providerValue: ScreenDimensions,
  renderOptions?: Omit<RenderOptions, "queries">
): RenderResult =>
  customRender(
    <ScreenSizeContext.Provider value={buildScreenSizeObject(providerValue)}>
      {ui}
    </ScreenSizeContext.Provider>,
    renderOptions
  )

// Mock IntersectionObserver
class IntersectionObserver {
  observe = jest.fn()
  disconnect = jest.fn()
  unobserve = jest.fn()
}

Object.defineProperty(window, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
})

Object.defineProperty(global, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
})
