enum Environment {
  CLIENT,
  SERVER,
}

const getRuntimeEnvironment = (): Environment =>
  typeof window === "undefined" ? Environment.SERVER : Environment.CLIENT

export const isRuntimeEnvironmentClient = (): boolean =>
  getRuntimeEnvironment() === Environment.CLIENT

export const isRuntimeEnvironmentServer = (): boolean =>
  getRuntimeEnvironment() === Environment.SERVER
